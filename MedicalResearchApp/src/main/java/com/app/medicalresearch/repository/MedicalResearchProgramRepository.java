package com.app.medicalresearch.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.medicalresearch.model.MedicalResearchProgram;

public interface MedicalResearchProgramRepository extends JpaRepository<MedicalResearchProgram, Integer>{

}
