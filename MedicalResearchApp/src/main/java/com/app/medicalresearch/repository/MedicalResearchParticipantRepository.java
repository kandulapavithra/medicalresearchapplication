package com.app.medicalresearch.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.medicalresearch.model.MedicalResearchParticipant;

public interface MedicalResearchParticipantRepository extends JpaRepository<MedicalResearchParticipant, Integer> {

}
