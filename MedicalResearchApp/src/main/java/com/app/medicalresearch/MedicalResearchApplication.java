package com.app.medicalresearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedicalResearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedicalResearchApplication.class, args);
	}

}
