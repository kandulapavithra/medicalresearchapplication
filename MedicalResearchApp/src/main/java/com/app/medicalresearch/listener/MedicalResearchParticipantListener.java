package com.app.medicalresearch.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.app.medicalresearch.model.MedicalResearchParticipant;

@Component
public class MedicalResearchParticipantListener {
	static final Logger logger = LogManager.getLogger(MedicalResearchParticipantListener.class.getName());

	@JmsListener(destination = "${medicalresearch.jms.topic}", containerFactory = "medicalresearchJmsFactory")
	public void getMedicalResearchParticipant1(MedicalResearchParticipant medicalResearchParticipant) {
		logger.info("MedicalResearchParticipant listener1: " + medicalResearchParticipant);
	}

	@JmsListener(destination = "${medicalresearch.jms.topic}", containerFactory = "medicalresearchJmsFactory")
	public void getMedicalResearchParticipant2(MedicalResearchParticipant medicalResearchParticipant) {
		logger.info("MedicalResearchParticipant Listener2: " + medicalResearchParticipant);
	}
	
}