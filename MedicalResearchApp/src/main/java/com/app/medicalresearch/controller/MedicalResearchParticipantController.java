package com.app.medicalresearch.controller;

import java.io.IOException;

import javax.jms.JMSException;
import javax.jms.Topic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.app.medicalresearch.model.MedicalResearchParticipant;
import com.app.medicalresearch.service.MedicalResearchParticipantService;

@RestController
@RequestMapping("/medicalResearchParticipant")
public class MedicalResearchParticipantController {
	static final Logger logger = LogManager.getLogger(MedicalResearchParticipantController.class.getName());

	@Autowired
	private MedicalResearchParticipantService medicalResearchParticipantService;

	@Autowired
	JmsTemplate jmsTemplate;

	/*
	 * Description: This end point is to get MedicalResearchParticipant details by
	 * id.
	 */
	@GetMapping({ "/{id}" })
	public MedicalResearchParticipant getMedicalResearchParticipantById(@PathVariable Integer id) {
		logger.info("Inside MedicalResearchParticipantController:getMedicalResearchParticipantById() method");
		return medicalResearchParticipantService.getMedicalResearchParticipantById(id);
	}

	/*
	 * Description: This end point is to save MedicalResearchParticipant details.
	 */
	@PostMapping
	public void saveMedicalResearchParticipantDetails(@RequestParam("file") MultipartFile file,
			@RequestBody MedicalResearchParticipant medicalResearchParticipantModel) throws IOException {
		logger.info("Inside MedicalResearchParticipantController:saveMedicalResearchParticipant() method");
		medicalResearchParticipantService.saveMedicalResearchParticipantDetails(file, medicalResearchParticipantModel);
	}

	/*
	 * Description: This end point is to update MedicalResearchParticipant details.
	 */
	@PutMapping({ "/{id}" })
	public void updateMedicalResearchParticipantDetails(@RequestParam("file") MultipartFile file,
			@RequestBody MedicalResearchParticipant medicalResearchParticipantModel, @PathVariable Integer id)
			throws IOException {
		logger.info("Inside MedicalResearchParticipantController:updateMedicalResearchParticipant() method");
		medicalResearchParticipantService.updateMedicalResearchParticipantdDetails(file,
				medicalResearchParticipantModel, id);
	}

	/*
	 * Description: This end point is for deleting MedicalResearchParticipant
	 * details by id.
	 */
	@DeleteMapping({ "/{id}" })
	public void deleteMedicalResearchParticipantDetailsById(@PathVariable Integer id) throws JMSException {
		logger.info("Inside MedicalResearchParticipantController:deleteMedicalResearchParticipantByID() method");
		Topic medicalResearchParticipantTopic = jmsTemplate.getConnectionFactory().createConnection().createSession()
				.createTopic("MedicalResearchTopic");
		logger.info("Sending MedicalResearchParticipant Object: " + id);
		jmsTemplate.convertAndSend(medicalResearchParticipantTopic, id);
		medicalResearchParticipantService.deleteMedicalResearchParticipantDetailsById(id);
	}

}