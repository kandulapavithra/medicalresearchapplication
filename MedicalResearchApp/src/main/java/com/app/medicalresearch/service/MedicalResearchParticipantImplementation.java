package com.app.medicalresearch.service;

import java.io.IOException;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.app.medicalresearch.model.MedicalResearchParticipant;
import com.app.medicalresearch.repository.MedicalResearchParticipantRepository;

@Service
public class MedicalResearchParticipantImplementation implements MedicalResearchParticipantService {

	static final Logger logger = LogManager.getLogger(MedicalResearchParticipantImplementation.class);

	@Autowired
	private MedicalResearchParticipantRepository medicalResearchParticipantRepository;

	@Override
	public MedicalResearchParticipant getMedicalResearchParticipantById(Integer id) {
		logger.info("Inside MedicalResearchParticipantImplementation:getMedicalResearchParticipantById() method");
		return medicalResearchParticipantRepository.getById(id);
	}

	@Transactional
	@Override
	public void saveMedicalResearchParticipantDetails(MultipartFile file, MedicalResearchParticipant medicalResearchParticipantModel) throws IOException {
		logger.info("Inside MedicalResearchParticipantImplementation:saveMedicalResearchParticipantDetails() method");
		medicalResearchParticipantModel.setData(file.getBytes());
		medicalResearchParticipantRepository.save(medicalResearchParticipantModel);
	}

	@Transactional
	@Override
	public void updateMedicalResearchParticipantdDetails(MultipartFile file, MedicalResearchParticipant medicalResearchParticipantModel,
			Integer id) throws IOException{
		logger.info(
				"Inside MedicalResearchParticipantImplementation:updateMedicalResearchParticipantdDetails() method");
		if (id == medicalResearchParticipantModel.getParticipantId()) {
			medicalResearchParticipantModel.setData(file.getBytes());
			medicalResearchParticipantRepository.save(medicalResearchParticipantModel);
		}

	}

	@Override
	public void deleteMedicalResearchParticipantDetailsById(Integer id) {
		logger.info("Inside EmployeeServiceImplementation:deleteMedicalResearchParticipantDetailsById() method");
		medicalResearchParticipantRepository.deleteById(id);
	}

}
