package com.app.medicalresearch.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.app.medicalresearch.model.MedicalResearchParticipant;

public interface MedicalResearchParticipantService {

	MedicalResearchParticipant getMedicalResearchParticipantById(Integer id);

	void saveMedicalResearchParticipantDetails(MultipartFile file,
			MedicalResearchParticipant medicalResearchParticipantModel) throws IOException;

	void updateMedicalResearchParticipantdDetails(MultipartFile file,
			MedicalResearchParticipant medicalResearchParticipantModel, Integer id) throws IOException;

	void deleteMedicalResearchParticipantDetailsById(Integer id);

}
