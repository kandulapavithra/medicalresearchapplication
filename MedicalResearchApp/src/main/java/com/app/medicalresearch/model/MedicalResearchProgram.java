package com.app.medicalresearch.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name = "medical_research_program")
@AllArgsConstructor
@NoArgsConstructor
public class MedicalResearchProgram {
	@Id
	@Column(name = "research_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer researchId;

	// biology, chemistry, toxicology
	private String researchFieldName;

	// specific research name
	private String researchName;

}