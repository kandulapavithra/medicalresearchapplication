package com.app.medicalresearch.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name = "medical_research_participant")
@AllArgsConstructor
@NoArgsConstructor
public class MedicalResearchParticipant {
	@Id
	@Column(name = "participant_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer participantId;

	private String participantName;

	private Integer age;

	@ManyToOne
	@JoinColumn(name = "research_id")
	private MedicalResearchProgram medicalResearchProgram;

	private Date researchStartDate;

	@Lob
	private byte[] data;
	
}